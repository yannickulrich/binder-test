# McMule user files

In this repository we collect all user files as well as results that
have been successfully used with McMule. Some of these results have
directly led to publications in which case the arXiv link will be
provided.

Data is sorted first by generic process and next by experiment or
configuration used. For published results, we include the user file,
the run configurations, the run results as well as the analysis code.

| process                          | order | experiments | comments                     | status / ref. |
| -------------------------------- | ----- | ----------- | ---------------------------- | ------------- |
|$`\mu\to\nu\bar\nu e`$            | NNLO  | MEG I & II  | polarised, massified & exact | published     |
|$`\mu\to\nu\bar\nu e\gamma`$      | NLO   | MEG I       | polarised                    | published     |
|$`\mu\to\nu\bar\nu eee`$          | NLO   | Mu3e        | polarised                    | published     |
|$`\mu\to\nu\bar\nu e\gamma\gamma`$| LO    | MEG         | polarised                    | priv. comm.   |
|$`\tau\to\nu\bar\nu e\gamma`$     | NLO   | BaBar       | cuts in lab frame            | published     |
|$`\tau\to\nu\bar\nu l\ell\ell`$   | NLO   | Belle II    |                              | in prep.      |
|$`e\mu\to e\mu`$                  | NLO   | MUonE       | complete                     | unpublished   |
|                                  | NNLO  |             | electronic                   | in prep.      |
|$`\ell p\to\ell p`$               | NNLO  | P2          | only leptonic                | in prep.      |
|                                  | NNLO  | Muse        | only leptonic                | in prep.      |
|$`e^-e^-\to e^-e^-`$              | NNLO  | Prad        |                              | to be done    |
|$`e^+e^-\to e^+e^-`$              | NNLO  |             |                              | to be done    |
|$`e^+e^-\to \gamma\gamma`$        | NNLO  | PADME       |                              | not started   |
|$`e^+e^-\to \mu^+\mu^-`$          | NNLO  | Belle       |                              | not started   |


## General information

This repository uses [LFS](https://git-lfs.github.com/) to manage
results. To prevent accidental changes, please activate locking
```bash
git config lfs.https://gitlab.com/mule-tools/user-library.git/info/lfs.locksverify true
```
