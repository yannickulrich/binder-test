import nbconvert
import nbformat
import nbformat.v4
import argparse
import os


def debug(s):
    pass


def py2nb(py):
    current = ["", ""]
    indent = 0
    nb = nbformat.v4.new_notebook()

    def push_cell():
        debug("[PUSH] current state is %s. Adding %d lines." % (
            current[0], len(current[1].splitlines())
        ))
        if current[0] == "":
            return
        elif current[0] == "comment":
            c = nbformat.v4.new_markdown_cell(
                current[1].rstrip().lstrip()
            )
        elif current[0] == "code":
            c = nbformat.v4.new_code_cell(
                current[1].rstrip().lstrip()
            )
        nb.cells.append(c)
        current[0] = ""
        current[1] = ""

    def add_cell(c, t):
        debug("[ADD ] Adding %d characters as %s" % (len(c), t))
        if current[0] != t:
            push_cell()
        current[0] = t
        current[1] += c + "\n"

    lines = py.splitlines()
    i = -1
    while i < len(lines) - 1:
        i += 1
        line = lines[i]
        if line == "# vim: foldmethod=marker":
            debug("[%4d] Skip vi header" % (i+1))
            continue
        if line.endswith("#########}}}"):
            debug("[%4d] Skip section end. Indent was %d" % (i, indent))
            indent -= 1
            push_cell()
            continue
        if len(line) == 0:
            debug("[%4d] empty line..." % (i+1))
            # Empty line, is next empty as well?
            if len(lines[i+1]) == 0:
                debug("[    ] next line empty, too. Push")
                i += 1
                push_cell()
            else:
                debug("[    ] next line not empty. Add")
                add_cell(line, 'code')
            continue
        if line[0] == "#":
            debug("[%4d] Comment line" % (i+1))
            c = line.strip('#').strip()
            if line.endswith("{{{"):
                debug("[    ] Section begin. Indent was %d" % indent)
                indent += 1
                c = "#"*indent + " " + c[:-3]

            add_cell(c, 'comment')
        if line[0] != "#":
            debug("[%4d] Code line" % (i+1))
            add_cell(line, 'code')

    push_cell()
    return nb


def executenb(nb, dir):
    pymule = 'from pymule import *'
    for i in range(len(nb.cells)):
        if pymule in nb.cells[i]['source']:
            break

    old = nb.cells[i]['source']
    nb.cells[i]['source'] = nb.cells[i]['source'].replace(
        pymule,
        pymule + "\nget_ipython().magic('matplotlib inline')"
    )

    ep = nbconvert.preprocessors.ExecutePreprocessor(
        timeout=600
    )
    ep.preprocess(nb, {'metadata': {'path': dir}})

    nb.cells[i]['source'] = old
    return nb


def nb2html(nb):
    html_exporter = nbconvert.HTMLExporter()
    (body, resources) = html_exporter.from_notebook_node(nb)
    return body


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'input', type=argparse.FileType('r'), help='input file'
    )
    parser.add_argument(
        '-o', type=argparse.FileType('w'), help='output file'
    )
    parser.add_argument(
        '-x', action='store_true', help="execute notebook"
    )
    parser.add_argument(
        '--html', action='store_true', help="convert result to html"
    )

    args = parser.parse_args()

    if args.o is None:
        if args.input.name == '<stdin>':
            name = 'stdin'
        else:
            name = os.path.splitext(args.input.name)[0]
        fp = open(name + ".ipynb", 'w')
    else:
        fp = args.o

    nb = py2nb(args.input.read())
    if args.x:
        print("Executing " + args.input.name)
        dir = os.path.dirname(name)
        if len(dir) == 0:
            dir = "."
        nb = executenb(nb, dir)

    nbformat.write(nb, fp)

    if args.html:
        html = nb2html(nb)
        with open(name + ".html", 'w') as fp:
            fp.write(html.encode("utf-8"))


main()
