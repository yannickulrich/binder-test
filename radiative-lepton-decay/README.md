# Radiative lepton decays $`L\to \nu\bar\nu l\gamma`$

References: [[1705.03782]](https://arxiv.org/abs/1705.03782), [[1712.05633]](https://arxiv.org/abs/1712.05633)

Configurations implemented

* Canonical cuts
 * MEG
   * full cuts
   * reduced cuts
 * PiBeta
 * BaBar
   * Canonical cuts
   * Full implantation
   * Only the energy
   * Invariant mass

In the radiative decay with massive fermions, an IR safe cut needs to
be applied to the hard photon. However, in the
$`\mathcal{M}_{n+1}^{(0)}`$ configuration there are two photons. The
hard one needs to be identified first and then cut.

## Canonical cuts
The canonical version refers to the values often quoted in the PDG of
```math
E_\gamma > 10\,\mathrm{MeV}
```
Usually, this also means that we are exclusive, i.e. exactly one
photon is visible. For the example for $`\tau\to\nu\bar\nu \gamma`$,
see [here](babar/example/)

## MEG

### Full cuts
In this configuration, the full MEG cuts are used. We assume a 85%
polarised muon and require the following cuts

```math
E_\gamma > 40\,\mathrm{MeV}\,,\qquad E_e> 45\,\mathrm{MeV}
```
```math
|\cos\sphericalangle(\vec p_\gamma,\vec e_z)| \equiv |\cos\theta_\gamma| < 0.35
\,,\qquad
|\phi_\gamma| < \frac{2\pi}{3}
```
```math
|\cos\sphericalangle(\vec p_e,\vec e_z)| \equiv |\cos\theta_e| < 0.5
\,,\qquad
|\phi_e| < \frac{\pi}{3}
```
MEG requires one and only one photon meaning that an event with real
radiation with energy larger then the detector resolution of roughly 2
MeV will be discarded if it hits the detector.


### Reduced cuts
It is also interesting to remove the cuts on the electron angles to
investigate to which extend the back-to-back approximation used by MEG
is valid.


## PiBeta cuts
The PiBeta experiment had less stringent cuts when looking at the
radiative muon decay, producing the current best result for this
process.

```math
E_\gamma > 10\,\mathrm{MeV}\,,\qquad \sphericalangle(\vec p_\gamma,\vec p_e) > 30^\circ
```


## BaBar

BaBar has measured the radiative tau decays in
[[1502.01784]](https://arxiv.org/pdf/1502.01784.pdf) with rather
complicated cuts.
