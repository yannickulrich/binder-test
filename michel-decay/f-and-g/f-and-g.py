# vim: foldmethod=marker
## Init{{{
from pymule import *
from pymule.plot import twopanel
import arbuzov
from scipy import optimize
import scipy.integrate as integrate
import scipy.stats

gamma0 = Mmu**5/192/pi**3

# We *define* $f$ and $g$ through
# \begin{align}
# \frac{\mathrm{d}^2\Gamma}{\mathrm{d}E_e\,\mathrm{d}(\cos\theta)}
#   = \Gamma_0\Big( f(E_e) + P\,\cos\theta\ g(E_e)\Big)\,.
# \end{align}
# At LO these are for $m=0$
# \begin{align}
#  f^{(0)}(E_e) &= (\tfrac{2E_e}{M})^2\big(3-2(\tfrac{2E_e}{M})\big)\,,\\
#  g^{(0)}(E_e) &= (\tfrac{2E_e}{M})^2\big(1-2(\tfrac{2E_e}{M})\big)\,.
# \end{align}
# We now calculate $f$ and $g$ by calculating with $P=0.85$
# \begin{align}
# \frac{\mathrm{d}\Gamma_-}{\mathrm{d} E_e}
#        =\int_{-1}^0\mathrm{d}(\cos\theta)\ \frac{\mathrm{d}^2\Gamma}
#                                    {\mathrm{d} E_e\,\mathrm{d}(\cos\theta)}
# \qquad\text{and}\qquad
# \frac{\mathrm{d}\Gamma_+}{\mathrm{d} E_e}
#        =\int_{ 0}^1\mathrm{d}(\cos\theta)\ \frac{\mathrm{d}^2\Gamma}
#                                    {\mathrm{d} E_e\,\mathrm{d}(\cos\theta)}
# \,.
# \end{align}
# Allowing us to obtain $f$ and $g$.
#
#
# To get a good enough sampling, we split the numerical $E_e$ integration in
# four integrals
# \begin{align}
#  \int_m^{E_\text{max}}\mathrm{d}E_e =
#  \int_{ 0}^{26}\mathrm{d}E_e
# +\int_{26}^{42}\mathrm{d}E_e
# +\int_{42}^{50}\mathrm{d}E_e
# +\int_{50}^{54}\mathrm{d}E_e
# \end{align}
# This is implemented in `user.f95` as follows
# ```fortran
#   ! 1st digit is cos sign (1 = -, 2 = +)
#   ! 2nd digit is the energy range
#
#   whatplot = mod(p_encrypted,10)
#
#   if(p_encrypted/10 == 1) then
#     whatplot = - whatplot
#   endif
#  ...
#   if(abs(whatplot) == 1) then
#      if(q2(4) > 26.) set_zero = 0
#    elseif(abs(whatplot) == 2) then
#      if(q2(4) < 26. .or. q2(4) > 42.) set_zero = 0
#    elseif(abs(whatplot) == 3) then
#      if(q2(4) < 42. .or. q2(4) > 50.) set_zero = 0
#    elseif(abs(whatplot) == 4) then
#      if(q2(4) < 50.) set_zero = 0
#    endif
#    if (whatplot > 0) then
#      ! We are in the cos > 0 region
#      if (cos_th(q2,ez) < 0) set_zero = 0
#    else
#      ! We are in the cos < 0 region
#      if (cos_th(q2,ez) > 0) set_zero = 0
#    endif
# ```
##########################################################################}}}
## Load data{{{
setup(folder='meg-2d/out.tar.bz2')


### Helper functions{{{
# These functions load the full dataset for a given sign
def lfunclo(s):
    return [
        scaleset(mergefks(
            sigma('m2enn0', obs=s+obs)
        ), alpha**0/gamma0)

        for obs in ['1', '2', '3', '4']
    ]


def lfuncnlo(s):
    return [
        scaleset(mergefks(
            sigma('m2ennR', obs=s+obs),
            sigma('m2ennF', obs=s+obs)
        ), alpha**1/gamma0)

        for obs in ['1', '2', '3', '4']
    ]


def lfuncnnlo(s):
    return [
        scaleset(mergefks(
            sigma(
                'm2ennRF', obs=s+obs,
                sanitycheck=lambda v:abs(v['value'])[0] < 1e10
            ),
            sigma('m2ennRR', obs=s+obs),
            sigma('m2ennFF', obs=s+obs)
        ), alpha**2/gamma0)

        for obs in ['1', '2', '3', '4']
    ]


# This functions merges the different runs for one sign into one large run that
# has full coverage of the spectrum
def mergesign(lfunc, s, name):
    sets = lfunc(s)

    dic = {
        'value': plusnumbers([i['value'] for i in sets]),
        'chi2a': sum(
            i['chi2a'][0][0] if type(i['chi2a'][0]) == list else i['chi2a'][0]
            for i in sets
        )/len(sets),
        'time': sum(i['time'] for i in sets)
    }
    for i in range(4):
        dic['Ee%d' % (i+1)] = sets[i]['Ee%d' % (i+1)][1:-1]

    return dic


###########################################################}}}
### Assemble $f$ and $g${{{
# We can now build $f$ and $g$ as
# \begin{align}
# f &= \frac{\Gamma_- + \Gamma_+}{2}\,,\\
# g &= \frac{-\Gamma_- + \Gamma_+}{0.85}
# \end{align}
def getfandg(lfunc, name):
    r1 = mergesign(lfunc, '1', name)
    r2 = mergesign(lfunc, '2', name)

    espec1 = np.concatenate((r1['Ee1'], r1['Ee2'], r1['Ee3'], r1['Ee4']))
    espec2 = np.concatenate((r2['Ee1'], r2['Ee2'], r2['Ee3'], r2['Ee4']))

    return (
        addplots(espec1, espec2, sa=0.5, sb=0.5),
        addplots(espec1, espec2, sa=-1/0.85, sb=1/0.85),
        r1['time'] + r2['time']
    )


flo, glo, tlo = getfandg(lfunclo, 'lo')
fnlo, gnlo, tnlo = getfandg(lfuncnlo, 'nlo')
fnnlo, gnnlo, tnnlo = getfandg(lfuncnnlo, 'nnlo')

Ee = flo[:, 0]
mask = (Mel < Ee) & (Ee < Mmu/2 + Mel**2/Mmu**2)

flo = flo[mask]
glo = glo[mask]
fnlo = fnlo[mask]
gnlo = gnlo[mask]
fnnlo = fnnlo[mask]
gnnlo = gnnlo[mask]

F = addplots(flo, addplots(fnlo, fnnlo))
G = addplots(glo, addplots(gnlo, gnnlo))
###########################################################}}}
##########################################################################}}}
## Compare at LO, NLO, and NNLO{{{
# Here we compare the results with Arbuzov
Ee = Ee[mask]

xe = 2*Ee/Mmu
beta = sqrt(1-Mel**2/Ee**2)

### Compare at LO{{{
fLOref = 2/Mmu * xe**2*beta * (3-2*xe+xe/4*(3*xe-4)*(1-beta**2))
gLOref = 2/Mmu * xe**2*beta * ((1-2*xe)*beta + 3*xe**2/4 * beta * (1-beta**2))


twopanel(
    "$E_e$", labupleft="$f_0$",
    upleft=[flo, np.column_stack((Ee, fLOref, np.zeros(Ee.shape)))],
    downleft=[np.column_stack((Ee, 1-flo[:, 1]/fLOref, flo[:, 2]/fLOref))]
)
ylim(-1e-3, 1e-3)


twopanel(
    "$E_e$", labupleft="$g_0$",
    upleft=[glo, np.column_stack((Ee, gLOref, np.zeros(Ee.shape)))],
    downleft=[np.column_stack((Ee, 1-glo[:, 1]/gLOref, glo[:, 2]/gLOref))]
)
ylim(-5e-2, 5e-2)
###########################################################}}}
### Compare at NLO{{{
fNLOref = alpha/(2*pi) * 2/Mmu * xe**2*beta * arbuzov.fnlo(xe, Mel/Mmu)
gNLOref = alpha/(2*pi) * 2/Mmu * xe**2*beta * arbuzov.gnlo(xe, Mel/Mmu)

twopanel(
    "$E_e$", labupleft="$f_1$",
    upleft=[fnlo, np.column_stack((Ee, fNLOref, np.zeros(Ee.shape)))],
    downleft=[np.column_stack((Ee, 1-fnlo[:, 1]/fNLOref, fnlo[:, 2]/fNLOref))]
)
ylim(-2e-3, 2e-3)
twopanel(
    "$E_e$", labupleft="$g_1$",
    upleft=[gnlo, np.column_stack((Ee, gNLOref, np.zeros(Ee.shape)))],
    downleft=[np.column_stack((Ee, 1-gnlo[:, 1]/gNLOref, gnlo[:, 2]/gNLOref))]
)
ylim(-2e-3, 2e-3)
###########################################################}}}
### Compare at NNLO{{{
L = np.log(Mmu**2/Mel**2)
pref2loop = (alpha/(2*pi))**2 * 2/Mmu
fNNLOref = pref2loop * (0.5*L**2*arbuzov.f2LL(xe) + L*arbuzov.f2NLL(xe))
gNNLOref = pref2loop * (0.5*L**2*arbuzov.g2LL(xe) + L*arbuzov.g2NLL(xe))

figure()
plot(Ee, fNNLOref)
errorband(fnnlo)
ylim(-2e-5, 5e-5)
xlabel("$E_e$")
ylabel("$f_2$")

figure()
plot(Ee, gNNLOref)
errorband(gnnlo)
ylim(-2e-5, 5e-5)
xlabel("$E_e$")
ylabel("$g_2$")
###########################################################}}}
##########################################################################}}}
## Beyond NNLO{{{
### Prologue{{{
# We now want to go beyond NNLO, especially towards the endpoint. For this we
# have two options:
#  * resum the endpoint using YFS and
#  * include the knwon N$^3$LO-LL terms.
# We start with the first point. Using Massimo's method, we expand $f_1/f_0$
# around the endpoint. We do this in the notation of [1811.06461] with $x =
# (p-q)^2/M^2$. We find
# \begin{align}
#  {\tt eplog} = \frac{f_1}{f_0} \sim \frac{g_1}{g_0} \sim
#  \frac{\alpha \log(x)}{\pi}\Bigg(
#    -2+\frac{1+z^2}{-1+z^2}\log\frac{m^2}{M^2}
#  \Bigg) + \text{regular as $x\to0$}\,,\\
# \end{align}

z = Mel / Mmu
eplogcoeff = alpha/pi*(
    -2. + (1.+z**2)/(-1.+z**2) * np.log(z**2)
)
# The resummed endpoint is now just $e^{\tt eplog} = 1+{\tt eplog} + \frac{{\tt
# eplog}^2}{2!}+\mathcal{O}((\alpha\log x)^3)$. Let us compare this at fixed
# order.
#################################################}}}
### Compare endpoint at fixed order{{{
# By re-expanding $e^{\tt eplog}$ we obtain only the $\log x$. To compare with
# McMule we need to add higher-order terms. Hence, we fit the McMule end-point
# as
# \begin{align}
#  f_1 &=            b\log x + c\ x^2+d\ x+e\,,\\
#  f_2 &= a\log^2x + b\log x + c\ x^2+d\ x+e\,.
# \end{align}
#### Fitting{{{


def fitfunc(p, Ee, o):
    x = 1. - 2*Ee/Mmu + (Mel/Mmu)**2
    ans = p[0] + p[1] * x + p[2] * x**2 + np.log(x) * p[3]
    if o == 2:
        ans += np.log(x)**2 * p[4]
    return ans


def fitep(data, o, lab):
    xdata = data[2730:, 0]
    ydata = data[2730:, 1]
    edata = data[2730:, 2]

    def errfunc(p, x, y, err):
        return (y - fitfunc(p, x, o)) / err

    pinit = [1.0] * (o+3)
    out = optimize.leastsq(errfunc, pinit,
                           args=(xdata, ydata, edata), full_output=1)

    coeff = out[0]
    covar = out[1]

    def band(Ee):
        cf = 0.95
        x = 1. - 2*Ee/Mmu + (Mel/Mmu)**2
        tval = scipy.stats.t.ppf((cf+1)/2, len(data) - len(coeff))
        if o == 1:
            xvec = np.array([1, x, x**2, np.log(x)])
        elif o == 2:
            xvec = np.array([1, x, x**2, np.log(x), np.log(x)**2])

        return tval*np.sqrt(np.matmul(np.matmul(xvec, covar), xvec))

    fitv = np.zeros(data[2730:].shape)
    fitv[:, 0] = xdata
    fitv[:, 1] = fitfunc(coeff, xdata, o)
    fitv[:, 2] = [band(e) for e in fitv[:, 0]]
    # One probably could calculate fitv[:,2] as well..

    twopanel(
        "$E_e$", labupleft=lab, labdownleft="res.",
        upleft=[data[2735:], fitv],
        downleft=[divideplots(data, fitv, offset=-1)]
    )
    return np.column_stack((coeff, np.sqrt(np.diag(covar))))


# Let's perform the fit
resf1 = fitep(divideplots(fnlo, flo), 1, "$f_1$")
ylim(-3e-4, 5e-4)
resg1 = fitep(divideplots(gnlo, glo), 1, "$g_1$")
ylim(-2e-3, 2e-3)


resf2 = fitep(divideplots(fnnlo, flo), 2, "$f_2$")
ylim(-2e-3, 2e-3)
resg2 = fitep(divideplots(gnnlo, glo), 2, "$g_2$")
ylim(-2e-2, 2e-2)
#################################################}}}
#### Compare with resummation{{{
print("Agreement for f1: " + printnumber(resf1[3]/eplogcoeff))
print("Agreement for g1: " + printnumber(resg1[3]/eplogcoeff))
print("Agreement for f2: " + printnumber(resf2[4]/(eplogcoeff**2/2)))
print("Agreement for g2: " + printnumber(resg2[4]/(eplogcoeff**2/2)))
#################################################}}}
###########################################################}}}
### Calculate YFS spectrum{{{
# We can now calculate the spectrum, including soft logarithms by taking the
# McMule data and adding the soft endpoint starting at $\alpha^3$.


def dointegral(integrand):
    diff = np.concatenate((
        0.026*np.ones(981),
        0.016*np.ones(1000),
        0.008*np.ones(1000),
        0.004*np.ones(706)
    ))

    return np.array([
        np.insert(
            np.array(integrate.quad(
                integrand, i, j, epsrel=1e-5
            )) / (j-i),
            0,
            (i + j)/2.
        )
        for i, j in np.column_stack((Ee-diff/2, Ee+diff/2))
    ])


def yfsintegrand(Ee, o):
    xe = 2*Ee/Mmu

    beta = sqrt(1-Mel**2/Ee**2)
    fLO = 2/Mmu * xe**2*beta * (3-2*xe+xe/4*(3*xe-4)*(1-beta**2))
    gLO = 2/Mmu * xe**2*beta * ((1-2*xe)*beta + 3*xe**2/4 * beta * (1-beta**2))

    logx = np.log(1. - xe + (Mel/Mmu)**2)
    eplog = logx * eplogcoeff
    epsub = np.exp(eplog) - (1. + eplog + eplog**2/2.)
    return [fLO * epsub, gLO * epsub][o]


fLLYFS = dointegral(lambda e: yfsintegrand(e, 0))
gLLYFS = dointegral(lambda e: yfsintegrand(e, 1))


FYFS = addplots(F, fLLYFS)
GYFS = addplots(G, gLLYFS)

###########################################################}}}
### Add Arbuzov's $(\alpha \log z)^3${{{
# Arbuzov's $(\alpha\log z)^3$ is
# \begin{align}
#   f_3&= \Big(\frac{\alpha}{2\pi}L\Big)^3\frac1{6}\ f_3^{(0,\gamma)} + ...
#   \,,\\
#  f_3^{(0,\gamma)} &= {\tt f3LL} \sim 8\times\log^3x + ...\,.
# \end{align}
# This is the term we have to avoid to not double-count the soft-collinear
# logarithm at $\alpha^3$.


def collintegrand(Ee, o):
    pref3loop = (alpha*L/(2*pi))**3 / 6.

    xe = 2*Ee/Mmu
    logx = np.log(1. - xe + (Mel/Mmu)**2)
    xa = 2*Ee*Mmu / (Mel**2 + Mmu**2)
    if o == 0:
        return (2./Mmu) * pref3loop * (arbuzov.f3LL(xa) - 8 * logx**3)
    else:
        return (2./Mmu) * pref3loop * (arbuzov.g3LL(xa) + 8 * logx**3)


f3LLsub = dointegral(lambda e: collintegrand(e, 0))
g3LLsub = dointegral(lambda e: collintegrand(e, 1))


FFULL = addplots(FYFS, f3LLsub)
GFULL = addplots(GYFS, g3LLsub)

###########################################################}}}
##########################################################################}}}
## Make plot{{{
### $f$ plot{{{
ffig, (ax1, ax2, ax3) = twopanel(
    labelx="$E_e$",
    upleft=[flo, FFULL],
    colupleft=['C2', 'C1'],
    labupleft="$F$",
    downleft=[divideplots(fnlo, flo, offset=1)],
    labdownleft={
        'ylabel': "$K^{(1)}$",
        'color': 'C0'
    },
    coldownleft=['C0'],
    downright=[
        divideplots(fnnlo, flo, offset=1),
        divideplots(
            addplots(fLLYFS, f3LLsub),
            flo,
            offset=1
        )
    ],
    labdownright={
        'ylabel': "$K^{(2,\\text{resum})}\\equiv f_i/f_0$",
        'color': 'C3'
    },
    coldownright=['C3', 'C1']
)
ax3.tick_params(labelcolor='C1', axis='y')

ax2.set_ylim(0.9, 1.52)
ax3.set_ylim(0.999, 1.0052)
ffig.legend(
    [
        matplotlib.lines.Line2D([0], [0], color=i) for i in [
            'C2', 'C0', 'C3', 'C1'
        ]
    ],
    [
        "$f_0$", "$f_1$", "$f_2$", "$f_\\text{resum}$"
    ],
    loc="upper center", ncol=4
)
mulify(ffig)


ffig.savefig("fplot.pdf")
###########################################################}}}
### $g$ plot{{{
gfig, (ax1, ax2, ax3) = twopanel(
    labelx="$E_e$",
    upleft=[glo, GFULL],
    colupleft=['C2', 'C1'],
    labupleft="$G$",
    downleft=[divideplots(gnlo, glo, offset=1)],
    labdownleft={
        'ylabel': "$K^{(1)}$",
        'color': 'C0'
    },
    coldownleft=['C0'],
    downright=[
        divideplots(gnnlo, glo, offset=1),
        divideplots(
            addplots(gLLYFS, g3LLsub),
            glo,
            offset=1
        )
    ],
    labdownright={
        'ylabel': "$K^{(2,\\text{resum})}\\equiv g_i/g_0$",
        'color': 'C3'
    },
    coldownright=['C3', 'C1']
)
ax3.tick_params(labelcolor='C1', axis='y')

ax2.set_ylim(0.58, 1.52)
ax3.set_ylim(0.996, 1.001)
gfig.legend(
    [
        matplotlib.lines.Line2D([0], [0], color=i) for i in [
            'C2', 'C0', 'C3', 'C1'
        ]
    ],
    [
        "$g_0$", "$g_1$", "$g_2$", "$g_\\text{resum}$"
    ],
    loc="upper center", ncol=4
)
mulify(gfig)


gfig.savefig("gplot.pdf")
###########################################################}}}
##########################################################################}}}
##########################################################################}}}
