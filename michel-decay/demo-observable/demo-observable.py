# vim: foldmethod=marker
from pymule import *
gamma0 = Mmu**5/192/pi**3

## Demo observable{{{
# We define an observable where we ban photons with $E_\gamma > 10\,{\rm
# MeV}$ within a cone of $|\cos\sphericalangle(\vec p_\gamma,\vec p_e)| >
# 0.8$
## Load data{{{
setup(folder='with-cut/out.tar.bz2', flavour='mu-e')


loC = scaleset(mergefks(sigma('m2enn0')), 1/gamma0)
fig, ans = mergefkswithplot([[sigma('m2ennF')], [sigma('m2ennR')]])
nloC = scaleset(ans, alpha/gamma0)
nnloC = scaleset(mergefks(
    sigma('m2ennRF'),
    sigma('m2ennRR'),
    sigma('m2ennFF', folder='../ff.tar.bz2')
), alpha**2/gamma0)

##########################################}}}
## Plot data{{{
fig, _ = kplot(
    {'lo': loC['xe'], 'nlo': nloC['xe'], 'nnlo': nnloC['xe']},
    labelx=r'$x_e$',
    labelsigma=r'$\D\sigma / \D x_e / \sigma_0$',
    legendopts={'what': 'u'},
    legend={
        'lo': r'$\sigma_0$',
        'nlo': r'$\sigma_1$',
        'nnlo': r'$\sigma_2$'
    }
)
savefig("demo.pdf")
##########################################}}}
##########################################################################}}}
