


                 !!!!!!!!!!!!!!!!!!!!!
                     MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!

  use functions

  implicit none

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

  integer, parameter :: nr_q = 2
  integer, parameter :: nr_bins = 200
  real, parameter ::                                                  &
     min_val(nr_q) = (/ 1./9.*pi  ,  0. /)
  real, parameter ::                                                  &
     max_val(nr_q) = (/ 5./9.*pi , 200. /)

  real(kind=prec) :: hcut,ycut

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

    !! ============================================== !!
    !! DO NOT EVEN THINK ABOUT CHANGING ANYTHING HERE !!
    !! ============================================== !!

  integer ::  set_zero(nr_q)
  character (len = 6), dimension(nr_q) :: names
  character(len=10) :: filenamesuffix


!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

            !! ----------------------------------------- !!
            !!     There are two versions of binning     !!
            !!     One for computing   d \sigma/ d Q     !!
            !!     One for computing  Q d \sigma/ d Q    !!
            !!  choose by setting the variable bin_kind  !!
            !! ----------------------------------------- !!
  real (kind=prec) :: bl_div = 1._prec
  integer :: bin_flag = 0       !!  0 for standard;  +1 for combined; 
  integer :: bin_kind = 0       !!  0 for d \sig/dQ; +1 for Q d \sig/dQ;


  contains




  SUBROUTINE FIX_MU

  !! ==== Specify the scale mu AND musq==mu**2 ==== !!

  mu = Mm

  musq = mu**2

  END SUBROUTINE FIX_MU



  SUBROUTINE USERINIT
  ! This is called without arguments once as soon as McMule
  ! starts and has read all other configuration, i.e. you can
  ! access which_piece and flavour. Use this to read any
  ! further information from the user (like cut configuration
  ! etc). You do not have to print the hashes - this is
  ! already taken care of - but you are very much invited to
  ! include information of what it is you are doing
  !
  ! If you are using the cut channel of the menu, you may need to set
  ! the filenamesuffix variable which is appended to the name of the
  ! VEGAS file.


  ! Example for reading a cut:
  ! integer cut

  integer inp

  read*, inp
  write(filenamesuffix,'(I4)') inp
  hcut = 10._prec**(-real(mod(inp, 100)))
  ycut = 10._prec**(-real(int(inp/ 100)))

  print*,"using hcut=",hcut,"and ycut=",ycut
  if (which_piece == 'em2emRREE') then
    print*,"Ignoring ycut though"
  endif
  END SUBROUTINE




  FUNCTION QUANT(q1,q2,q3,q4,q5,q6,q7)

  real (kind=prec), intent(in) :: q1(4),q2(4),q3(4),q4(4)
  real (kind=prec), optional :: q5(4),q6(4),q7(4)
  real (kind=prec) :: qq5(4),qq6(4),qq7(4),q1Rest(4),q2Rest(4),q3Rest(4),q4Rest(4)
  real (kind=prec) :: quant(nr_q)
  real (kind=prec) :: thetal, qsq

  qq5 = 0._prec
  qq6 = 0._prec
  qq7 = 0._prec

  if(present(q5)) qq5=q5
  if(present(q6)) qq6=q6
  if(present(q7)) qq7=q7

!  pol1 = (/ 0._prec, 0._prec, 0.85_prec, 0._prec /)
  pol1 = (/ 0._prec, 0._prec, 0._prec, 0._prec /)

  !! ==== keep the line below in any case ==== !!
  call fix_mu

  q1Rest = boost_rf(q2,q1)
  q2Rest = boost_rf(q2,q2)
  q3Rest = boost_rf(q2,q3)
  q4Rest = boost_rf(q2,q4)
  thetal = acos(cos_th(q1Rest,q3Rest))
  qsq = -sq(q2-q4)
  set_zero = 1
  if (flavour=="muse") then
    if (thetal.lt.  pi/9.) set_zero=0
    if (thetal.gt.5*pi/9.) set_zero=0
  elseif (flavour=="musee") then
    if (thetal.lt.  pi/9.) set_zero=0
    if (thetal.gt.5*pi/9.) set_zero=0
  elseif(flavour=="mesa") then
    if (thetal.lt.5./36.*pi) set_zero=0
    if (thetal.gt.1./ 4.*pi) set_zero=0
    if (q3rest(4) .lt. 45.) set_zero=0
  endif

  names(1) = "angle"
  quant(1) = thetal
  names(2) = "energy"
  quant(2) = q3rest(4)


  ! move variable out of bounds if cut has been applied
  quant = quant + 2*max_val*(1-set_zero)

  END FUNCTION QUANT






                 !!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!!!



