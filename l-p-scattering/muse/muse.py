# vim: foldmethod=marker
from pymule import *
## $e^-$-$p$-scattering $p=115\,{\rm MeV}${{{
# As the demostration, we calculate electronic MUSE with $p=115\,{\rm MeV}$.
### Load data{{{
setup(folder='e115/out.tar.bz2')

lo = scaleset(mergefks(sigma('mp2mp0')), conv*alpha**2)
nlo = scaleset(mergefks(
    sigma('mp2mpR'), sigma('mp2mpF')
), conv*alpha**3)
nnlo = scaleset(mergefks(
    sigma('mp2mpRR'), sigma('mp2mpRF15'), sigma('mp2mpRF35'),
    sigma('mp2mpFF')
), conv*alpha**4)
###########################################################}}}
### Make $\theta$-plot{{{
p0 = np.array([180/pi, 1., 1.]) * mergebins(lo['angle'], 5)
p1 = np.array([180/pi, 1., 1.]) * mergebins(nlo['angle'], 5)
p2 = np.array([180/pi, 1., 1.]) * mergebins(nnlo['angle'], 5)


fig, (ax1, ax2, ax3) = kplot(
    {
        'lo': p0, 'nlo': p1, 'nnlo': p2
    },
    labelx=r'$\theta_e$',
    labelsigma=r'$\D\sigma/\D\theta_e\ /\ {\rm\upmu b}$',
    legend={
        'lo': '$\\sigma^{(0)}$',
        'nlo': '$\\sigma^{(1)}$',
        'nnlo': '$\\sigma^{(2)}$'},
    legendopts={'what': 'u', 'loc': 'upper right'}
)
ax1.xaxis.set_major_formatter(FormatStrFormatter("$%d^\\circ$"))
ax1.set_xlim(2, 100)
ax1.set_yscale('log')
ax2.set_ylim(1.0, 2.1)
ax3.set_ylim(1.0, 1.0052)
fig.savefig('e115.pdf')


###########################################################}}}
#####################################################################}}}
